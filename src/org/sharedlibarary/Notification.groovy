#!/usr/bin/env groovy
package org.sharedlibrary;

class Notification{
	def notifyStarted(){
		
		sh '''
echo -e '{"text":'`echo ${JOB_NAME}`' job now started. Check console output at this URL '`echo ${BUILD_URL}consoleText`'"}' | curl -X POST -H 'Content-type: application/json' --data-binary @- https://franconnect.ryver.com/application/webhook/WOU7MJxgJCRLqaB#
'''
		println "notify started"
		
		}
		def notifySuccessful(){
		
		sh '''
echo -e '{"text":'`echo ${JOB_NAME}`' job successfully completed.
'''
		
		
		}
		
		def notifyFailed(){
		
		sh '''

echo -e '{"text":'`echo ${JOB_NAME}`' job failed.

'''
		println "notify failed"
		
		
		}
	
}